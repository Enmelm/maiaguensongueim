
var canvas = document.getElementById('canvas');
var CANVAS_WIDTH = canvas.width;
var CANVAS_HEIGHT = canvas.height;
var ACELERACION_BASE = 1.5;
var FRICCION_BASE = 0.0250;
var ctx = canvas.getContext('2d');

var cursor = {
    x: 0, 
    y:0,
    aceleration: {
        x: 0,
        y: 0,
    },
};

ctx.fillStyle = 'rgba(0, 0, 200, 0.5)';
ctx.strokeStyle = 'rgba(0, 0, 200, 0.5)';
ctx.fillRect(cursor.x, cursor.y, 50, 50);

var key;

document.addEventListener('keydown', e => {
    key = e.key;
}, false); 

window.requestAnimationFrame(repaint);

function repaint() {
    window.requestAnimationFrame(repaint);
    paint(ctx);
}

function paint() {

    ctx.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    ctx.strokeRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
    ctx.fillRect(cursor.x, cursor.y, 50, 50);
    ctx.fillText('Aceleracion-x: ' + cursor.aceleration.x, 10, 10);
    ctx.fillText('Aceleracion-y: ' + cursor.aceleration.y, 10, 20);
    ctx.fillText('x: ' + parseInt(cursor.x), 200, 10);
    ctx.fillText('y: ' + parseInt(cursor.y), 250, 10);

}

function act() {

    if(key == 'ArrowDown'){
        cursor.aceleration.y += ACELERACION_BASE;
    }
    
    if(key == 'ArrowUp'){
        cursor.aceleration.y -= ACELERACION_BASE;
    }

    if(key == 'ArrowLeft'){
        cursor.aceleration.x -= ACELERACION_BASE;
    }

    if(key == 'ArrowRight'){
        cursor.aceleration.x += ACELERACION_BASE;
    }

    if (cursor.x >= (CANVAS_WIDTH - 50)) {
        if(cursor.aceleration.x > 0){
            cursor.aceleration.x = cursor.aceleration.x * -1;
        }
    }

    if (cursor.x <= 0) {
        if(cursor.aceleration.x < 0){
            cursor.aceleration.x = cursor.aceleration.x * -1;
        }
    }

    if (cursor.y >= (CANVAS_HEIGHT - 50)) {
        if(cursor.aceleration.y > 0){
            cursor.aceleration.y = cursor.aceleration.y * -1;
        }
    }

    if (cursor.y <= 0) {
        if(cursor.aceleration.y < 0){
            cursor.aceleration.y = cursor.aceleration.y * -1;
        }
    }

    if(cursor.aceleration.y > 50){
        cursor.aceleration.y = 50;
    }

    if(cursor.aceleration.x > 50){
        cursor.aceleration.x = 50;
    }

    cursor.y += cursor.aceleration.y;
    cursor.x += cursor.aceleration.x;

    direction();
    key = null;
}

function direction() {

    var acel = cursor.aceleration;

    if(acel.x > 0){

        if (acel.x < 1) {
            cursor.aceleration.x = 0;
        }else{
            cursor.aceleration.x -= cursor.aceleration.x * FRICCION_BASE;
        }
    }

    if(acel.x < 0){

        if (acel.x > -1) {
            cursor.aceleration.x = 0;
        }else{
            cursor.aceleration.x -= cursor.aceleration.x * FRICCION_BASE;
        }
    }

    if(acel.y > 0){

        if (acel.y < 1) {
            cursor.aceleration.y = 0;
        }else{
            cursor.aceleration.y -= cursor.aceleration.y * FRICCION_BASE;
        }
    }

    if(acel.y < 0){

        if (acel.y > -1) {
            cursor.aceleration.y = 0;
        }else{
            cursor.aceleration.y -= cursor.aceleration.y * FRICCION_BASE;
        }
    }
}

(function run() {
    window.requestAnimationFrame(run);
    act();
    paint();
})();

  

